#!/bin/bash

command_exists () {
    type "$1" &> /dev/null ;
}

ENVIRONMENT=$2
SITE=$1

if command_exists drush; then
  GIT_REPOSITORY=`drush sa @$1.$2 --fields=\#upstream --format=list`;
fi

GIT_REPOSITORY=$3
BASE=/var/www/${SITE}.${ENVIRONMENT}

if [ $# -ne 3 ]; then
  echo "Usage: $0 [site] [environment] [git repository]"
  exit
fi

if [ -d "${BASE}/working" ]; then
  echo "The working directory already exists. Please remove it before running rein-clone.sh"
  exit
fi

# If the base directory doesn't exist we will setup the whole structure.
# If the directories get removed we'll need to either manually create them or
# remove the whole base directory and re-create it.
if [ ! -d "${BASE}" ]; then
  mkdir ${BASE}
  mkdir ${BASE}/settings
  mkdir ${BASE}/files
fi

cd ${BASE}

git clone ${GIT_REPOSITORY} ${BASE}/working
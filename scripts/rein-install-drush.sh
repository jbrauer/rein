#!/bin/bash

INSTALL_DIRECTORY=/usr/local/src
LINK_DIRECTORY=/usr/local/bin
DRUSH_VERSIONS_TO_INSTALL="7 8"
DRUSH_GIT_REPOSITORY=https://github.com/drush-ops/drush.git


## Determine the path of our own script to know where REIN's scripts are.##
## http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in ##
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
REIN_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

command_exists () {
  type "$1" &> /dev/null ;
}

## Pre-installation checks ##

if [ ! -w  ${INSTALL_DIRECTORY}  ]; then
  echo "This script requires being able to write to ${INSTALL_DIRECTORY}."
  exit
fi

if [ ! -w  ${LINK_DIRECTORY}  ]; then
  echo "This script requires being able to write to ${LINK_DIRECTORY}."
  exit
fi

if ! command_exists composer; then
  cd /tmp
  curl -sS https://getcomposer.org/installer | php
  mv composer.phar /usr/local/bin/composer
fi

if ! command_exists git; then
  echo "This command requires git to be installed and executable to complete the installation of the drush repositories";
  exit 1;
fi

if command_exists drush; then
  echo "Drush is already installed please remove it before contuning.";
  exit 1;
fi

if [ ! -f "${REIN_DIR}/rein-drush.sh" ]; then
  echo "The rein-drush.sh script was not found in this directory please make sure the rein installation is complete before continuing.";
  exit 1;
fi

## Install drush versions ##
git clone ${DRUSH_GIT_REPOSITORY} ${INSTALL_DIRECTORY}/drush_repo
for i in ${DRUSH_VERSIONS_TO_INSTALL}
do
  cp -a ${INSTALL_DIRECTORY}/drush_repo ${INSTALL_DIRECTORY}/drush${i}
  cd ${INSTALL_DIRECTORY}/drush${i}
  if [ ${i} -ne 9 ]; then
      git checkout ${i}.x
  else
    git checkout master
  fi
  composer install
  ln -s ${INSTALL_DIRECTORY}/drush${i}/drush ${LINK_DIRECTORY}/drush${i}
  echo "Installed ${LINK_DIRECTORY}/drush${i}"
done

## Install the link to our drush wrapper script. ##

ln -s ${REIN_DIR}/rein-drush.sh ${LINK_DIRECTORY}/drush
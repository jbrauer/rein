#!/bin/bash
VERSION=$3
ENVIRONMENT=$2
SITE=$1
BASE=/var/www/${SITE}.${ENVIRONMENT}
DIR=${BASE}/releases/${VERSION}

if [ $# -le 2 ]; then
  echo "Usage: $0 [site] [environment] [version number] "
  exit
fi

if [ -d "${BASE}/working" ]; then
  # @todo Run a backup script of the database before changes are made.

  cd ${BASE}/working
  git fetch
  git reset --hard
  git checkout ${VERSION}
  git checkout-index -a --prefix=${DIR}/

  # Link the working directories in the release.
  cd ${DIR}
  ln -sfn ${BASE}/files docroot/sites/default/
  ln -sfn ${ENVIRONMENT}/env.settings.inc envsettings/env.settings.inc
  ln -sfn ${BASE}/settings/db.settings.inc envsettings/db.settings.inc

  # Change the current link to point to the new release.
  ln -fsn ${DIR} ${BASE}/current

else
  echo "The site and environment working directory does not exist yet please rein-clone.sh first."
fi

#!/bin/bash

## Credit for this idea goes to http://www.triquanta.nl/blog/automatically-switch-drush-versions-project ##
## Allow sites to define in the local working repository what version of Drush they need to use.
## Also set the default in the user's config by defining DEFAULT_DRUSH_VERSION.

## Make sure this command is not run in a docroot in case there is an error executing.
DEFAULT_DRUSH=`cd /; drush8 ev 'echo DEFAULT_DRUSH_VERSION'`
if [ -z "$DEFAULT_DRUSH" ]; then
    DEFAULT_DRUSH=7
fi

version=$(git config --get drush.version)
if [ "$version" = '7' ];
then
    drush7 "$@"
else
    drush${DEFAULT_DRUSH} "$@"
fi
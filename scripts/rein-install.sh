#!/bin/bash

INSTALL_DIRECTORY=/usr/local/src
LINK_DIRECTORY=/usr/local/bin
DRUSH_INSTALL_SCRIPT=rein-install-drush.sh


## Determine the path of our own script to know where REIN's scripts are.##
## http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in ##
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
REIN_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

command_exists () {
  type "$1" &> /dev/null ;
}

if ! command_exists drush; then
  ${REIN_DIR}/${DRUSH_INSTALL_SCRIPT}
fi

ln -s ${REIN_DIR}/rein-clone.sh ${LINK_DIRECTORY}/rein-clone
ln -s ${REIN_DIR}/rein-deploy.sh ${LINK_DIRECTORY}/rein-deploy

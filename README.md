# README #

The Rein project is a set of scripts to use bash shell, git and drush commands to deploy Drupal sites for development, testing and production sites.

Some initial ramblings about this project can be found here http://joshuabrauer.com/2014/04/drush-and-cloning

Also incorporates some great ideas found at http://www.triquanta.nl/blog/automatically-switch-drush-versions-project

### Scripts

Rein is made up of several scripts. For the time being it really prefers to be installed in /usr/local/src/rein and the scripts in /usr/local/src/rein/scripts linked to /usr/local/bin ... this assumes you've added /usr/local/bin to the $PATH in your shell.

The scripts are:

*  rein-clone.sh -- clones a Drupal site repository to set it up for local deployments.

*  rein-deploy.sh -- deploys a given tag (also works with a SHA or less well with a branch because it can't overwrite the branch the next time around)

*  rein-drush.sh -- a wrapper for drush that allows sites to specify locally whattheir preferred version is. Designed to be used with rein-install-drush.sh or at least local installs of drush6 and drush7

*  rein-install.sh -- handles the setup of rein scripts. It will run rein-install-drush.sh as well so it won't be necessary to run that separately.

*  rein-install-drush.sh -- downloads and installs drush 6 and drush 7 heads and links them to /usr/local/bin as well as installing the rein-drush wrapper as the main 'drush' command